//
//  FirstViewController.swift
//  MyLocations
//
//  Created by Albert Adisaputra on 01.06.17.
//  Copyright © 2017 Albert Adisaputra. All rights reserved.
//

import UIKit
import CoreLocation

class CurrentLocationViewController: UIViewController, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var messegeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var tagButton: UIButton!
    @IBOutlet weak var getButton: UIButton!

    let locationManager = CLLocationManager()
    var location: CLLocation?
    var updatingLocation = false
    var lastLocationError: NSError?
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    var performingReverseGeocoding = false
    var lastGeocodingError: NSError?
    
    
    
    @IBAction func getLocation(_ sender: Any) {
        let authStatus = CLLocationManager.authorizationStatus()

        if authStatus == .denied || authStatus == .restricted{
            showLocationServicesDeniedAlert()
            return
        }
        
        if authStatus == .notDetermined{
            locationManager.requestWhenInUseAuthorization()
            return
        }
        if updatingLocation{
            stopLocationManager()
        }else {
            location = nil
            lastLocationError = nil
            placemark = nil
            lastGeocodingError = nil
            startLocationManager()
        }
        updateLabels()
        configureGetButton()
    }
    
    
    
    func updateLabels(){
        if let location = location {
            latitudeLabel.text =
                String(format: "%.8f", location.coordinate.latitude)
            longitudeLabel.text =
                String(format: "%.8f", location.coordinate.longitude)
            tagButton.isHidden = false
            messegeLabel.text = ""
            if let placemark = placemark{
                addressLabel.text = stringFromPlacemark(placemark: placemark)
            } else if performingReverseGeocoding {
                    addressLabel.text = "Searching for Adress..."
                }
            else if lastGeocodingError != nil {
                addressLabel.text = "Error Finding Adress"
                }
            else{
                addressLabel.text = "No Address Found"
            }
        }
        else{
            latitudeLabel.text = ""
            longitudeLabel.text = ""
            tagButton.isHidden = true
            
            let statusMessege: String
            if let error = lastLocationError{
                if error.domain == kCLErrorDomain && error.code == CLError.denied.rawValue{
                    statusMessege = "Location Services Disabled"
                } else{
                    statusMessege = "Error Getting Location"
                }
            } else if !CLLocationManager.locationServicesEnabled(){
                statusMessege = "Location Services Disabled"
            } else if updatingLocation{
                statusMessege = "Searching..."
            } else {
                statusMessege = "Tap 'Get My Location' to Start"
            }
            messegeLabel.text = statusMessege
        }
    }
    
    func stringFromPlacemark(placemark: CLPlacemark) -> String{
        var line1 = ""
        if let s = placemark.subThoroughfare{
            line1 += s + " "
        }
        if let s = placemark.thoroughfare{
            line1 += s
        }
        
        var line2 = ""
        
        if let s = placemark.locality{
            line2 += s + " "
        }
        if let s = placemark.administrativeArea{
            line2 += s + " "
        }
        if let s = placemark.postalCode{
            line2 += s
        }
        
        return line1 + "\n" + line2
        
    }
    
    func showLocationServicesDeniedAlert(){
        let alert = UIAlertController (title: "Location Services Disabled", message: "Please enable location services for this app in Settings", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func stopLocationManager(){
        if updatingLocation{
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            updatingLocation = false
        }
    }
    
    func startLocationManager(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            updatingLocation = true
        }
    }
    
    func configureGetButton(){
        if updatingLocation{
            getButton.setTitle("Stop", for: .normal)
        }else{
            getButton.setTitle("Get My Location", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLabels()
        configureGetButton()
        // Do any additional setup after loading the view, typically from a nib.
        updateLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func locationManager(manager: CLLocationManager, didFailWithError error : NSError){
        print("didFailWithError \(error)")
        if error.code == CLError.locationUnknown.rawValue{
            return
        }
        
        lastLocationError = error
        stopLocationManager()
        updateLabels()
        configureGetButton()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation = locations.last!
        print("didUpdateLocations \(newLocation)")
        
        if newLocation.timestamp.timeIntervalSinceNow < -5{
            return
        }
        
        if newLocation.horizontalAccuracy < 0 {
            return
        }
        if location == nil || location!.horizontalAccuracy < newLocation.horizontalAccuracy{
            
        
            lastLocationError = nil
            location = newLocation
            updateLabels()
            if newLocation.horizontalAccuracy <= locationManager.desiredAccuracy {
                print("*** We are done!")
                stopLocationManager()
                configureGetButton()
            }
            if !performingReverseGeocoding{
                print ("*** Going to Geocode")
                
                performingReverseGeocoding = true
                geocoder.reverseGeocodeLocation(newLocation, completionHandler: {placemarks, error in
                print ("*** Found placemarks: \(placemarks), error: \(error)")
                    self.lastLocationError = error as? NSError
                    if error == nil, let p = placemarks, !p.isEmpty{
                        self.placemark = p.last!
                    }else{
                        self.placemark = nil
                    }
                    
                    self.performingReverseGeocoding = false
                    self.updateLabels()
                })
                }
            }
        }
        
    }
    


